variable "allocated_storage" {
  default = 25
}
variable "engine" {
    default = "postgres"
}
variable "vpc" {
  type = string

}