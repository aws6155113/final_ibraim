
resource "aws_db_instance" "default" {
  allocated_storage   = var.allocated_storage
  db_name             = "ibraimdb"
  engine              = var.engine
  engine_version      = "14.6"
  instance_class      = "db.t3.micro"
  username            = "ibraim"
  password            = "ibraim49"
  storage_type        = "gp2"
  identifier = "ibraim"
  publicly_accessible = true
  skip_final_snapshot = true
  # vpc_security_group_ids = [
  #   aws_security_group.rds-sg.id,]
  # db_subnet_group_name = aws_db_subnet_group.default
}
# resource "aws_security_group" "rds-sg" {
#   vpc_id      = var.vpc
#   description = "Allow access to PostgreSQL database from private network."
#   name        = "rdssg"
#   ingress {
#     from_port = 0
#     to_port   = 65535
#     protocol  = "tcp"

#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }
# resource "aws_db_subnet_group" "default" {
#   name       = "main"
#   subnet_ids = [aws_subnet.frontend.id, aws_subnet.backend.id]

#   tags = {
#     Name = "My DB subnet group"
#   }
# }
