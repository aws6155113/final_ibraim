terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.62.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"

}
module "dev-vpc" {
  env                  = "dev"
  source               = "../child_module/vpc"
  vpc_cidr             = var.vpc_cidr_block
  public_subnet_cidrs  = slice(var.public_subnet_cidr_blocks, 13, 15)
  private_subnet_cidrs = slice(var.private_subnet_cidr_blocks, 13, 16)
}
module "dev-eks" {
  source          = "../child_module/eks"
  env             = "dev"
  vpc             = module.dev-vpc.vpc_id
  subnet          = module.dev-vpc.private_subnet_id
  cluster_name    = var.eks_name
  cluster_version = var.eks_version
  # ami_id          = data.aws_ami.eks_default.id
  node_groups = {
    cluster1 = {
      node_group_name = "test"
      desired_size    = 4
      max_size        = 5
      min_size        = 4

      ami_type = "AL2_x86_64"
    },
  }
}

data "aws_ami" "eks_default" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amazon-eks-node-${var.eks_version}-v*"]
  }
}
module "rds" {
    source = "../child_module/rds"
    allocated_storage = 25
    vpc   = module.dev-vpc.vpc_id
    
    engine = "postgres"

}
